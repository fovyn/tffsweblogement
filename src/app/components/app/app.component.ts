import { SessionService } from './../../modules/user/services/session.service';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserSession } from 'src/app/modules/user/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  user$: Observable<string>;
  userSubscription: Subscription;

  constructor(private sessionService: SessionService) {}

  ngOnInit() {
    this.user$ = this.sessionService.token$;
    this.userSubscription = this.user$.subscribe(user => console.log(`User = ${user}`));

    // this.sessionService.addProductToBasket({prodId: 1, label: "Téléphone", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 2, label: "Téléphone1", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 3, label: "Téléphone2", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 4, label: "Téléphone3", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 5, label: "Téléphone4", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 6, label: "Téléphone5", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 7, label: "Téléphone6", qtt: 42});
    // this.sessionService.addProductToBasket({prodId: 1, label: "Téléphone7", qtt: 42});
  }
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onClickAction(event: MouseEvent) {
    console.log(event);
  }

  logoutAction(event: MouseEvent) {
    event.preventDefault();

    this.sessionService.logout();
  }

  addProduct(event: MouseEvent) {
    event.preventDefault();

    this.sessionService.addProductToBasket({prodId: 25, label: "Ajouter", qtt: 84});
  }
}
