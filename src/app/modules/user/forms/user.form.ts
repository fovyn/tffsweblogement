import { AbstractControl, FormControl, Validators } from '@angular/forms';

export type FormGroupDef = {[key: string]: AbstractControl};

export const LOGIN_FORM: FormGroupDef = {
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required])
};