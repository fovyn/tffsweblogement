import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    return this.httpClient.post<any>(`https://localhost:8000/api/login_check`, {username, password}).pipe(map(data => data.token));
  }

  roles() {
    return this.httpClient.get<any>(`https://localhost:8000/api/security/roles`).pipe(map(data => data.roles));
  }
}
