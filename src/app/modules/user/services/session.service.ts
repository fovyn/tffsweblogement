import { UserSession } from './../models/user.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BasketSession, BasketSessions } from '../models/basket.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _user$: BehaviorSubject<UserSession> = new BehaviorSubject<UserSession>(null);
  private _basket$: BehaviorSubject<BasketSessions> = new BehaviorSubject<BasketSessions>([]);
  private _token$: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private _roles$ = new BehaviorSubject<any[]>([]);

  constructor(private router: Router) { }

  get user$(): Observable<UserSession> {
    return this._user$.asObservable();
  }
  get user(): UserSession | null {
    return this._user$.value;
  }

  get basket$(): Observable<BasketSessions> {
    return this._basket$.asObservable();
  }
  get basket(): BasketSessions {
    return this._basket$.value;
  }

  get token$(): Observable<string> {
    return this._token$.asObservable();
  }
  get token(): string {
    return this._token$.value;
  }

  get roles$(): Observable<any[]> {
    return this._roles$.asObservable();
  }

  login(token: string) {
    this._token$.next(token);
  }

  logout() {
    this._token$.next(null);
    this.router.navigate(['/']);
  }

  addProductToBasket(basketSession: BasketSession) {
    const currentValue = this.basket;
    //Vérification de l'existence d'un produit dans le panier
    const currentItem = currentValue.find(bs => bs.prodId == basketSession.prodId);

    //Si il existe
    if (currentItem) {
      currentItem.qtt += basketSession.qtt;
      //Refresh des data
      this._basket$.next(currentValue);
    } else {
      //Ajoute une nouvelle donnée
      this._basket$.next([...currentValue, basketSession]);
    }

    sessionStorage.setItem("BASKET", JSON.stringify(this._basket$.value));
    localStorage.setItem("BASKET", JSON.stringify(this._basket$.value));
  }

  setRoles(roles: any[]) {
    this._roles$.next(roles);
  }
}
