import { SharedModule } from './../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [UserComponent, LoginComponentComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    HttpClientModule
  ]
})
export class UserModule { }
