import { AuthService } from './../../services/auth.service';
import { SessionService } from './../../services/session.service';
import { LOGIN_FORM } from './../../forms/user.form';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as M from 'materialize-css';
import { Router } from '@angular/router';

@Component({
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss']
})
export class LoginComponentComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private sessionService: SessionService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group(LOGIN_FORM);
  }

  onSubmitAction() {
    if (this.loginForm.valid) {
      const {username, password} = this.loginForm.value;
      // this.authService.createNewUser().subscribe(data => console.log(data));
      this.authService.login(username, password).subscribe(user => {
          this.sessionService.login(user);
          this.authService.roles().subscribe(roles => this.sessionService.setRoles(roles));
          M.toast({html: '<p>Connexion réussie</p>'});
          this.router.navigate(['/logements']);
      });
    }
  }

}
