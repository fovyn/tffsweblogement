
export interface BasketSession {
    prodId: number;
    label: string;
    qtt: number;
}

export type BasketSessions = BasketSession[];