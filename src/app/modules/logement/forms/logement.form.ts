import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AddressForm } from './address.form';
import { FormType } from './form-type.form';
export const LogementForm: FormType = {
    "name": new FormControl(null, [Validators.required]),
    "priceByMonth": new FormControl(0, [Validators.required])
}

export const LogementWithAddressForm: FormType = {
    "logement": new FormGroup(LogementForm, [Validators.required]),
    "address": new FormGroup(AddressForm)
}