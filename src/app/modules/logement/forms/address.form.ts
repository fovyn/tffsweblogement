import { FormControl, Validators } from '@angular/forms';
import { FormType } from './form-type.form';
export const AddressForm: FormType = {
    "number": new FormControl(null, [Validators.required]),
    "street": new FormControl(null, [Validators.required]),
    "city": new FormControl(null, [Validators.required]),
    "country": new FormControl(null, [Validators.required])
}