import { AbstractControl } from "@angular/forms";

export type FormType = {[key: string]: AbstractControl};