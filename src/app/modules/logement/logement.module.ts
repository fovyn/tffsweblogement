import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogementRoutingModule } from './logement-routing.module';
import { LogementComponent } from './components/logement/logement.component';
import { ListComponent } from './components/list/list.component';
import { MenuComponent } from './components/menu/menu.component';
import { SharedModule } from '../shared/shared.module';
import { CreateComponent } from './components/create/create.component';


@NgModule({
  declarations: [LogementComponent, ListComponent, MenuComponent, CreateComponent],
  imports: [
    CommonModule,
    LogementRoutingModule,
    SharedModule
  ]
})
export class LogementModule { }
