import { HttpClient } from '@angular/common/http';
import { MTabsDirective } from './../../../shared/directives/m-tabs.directive';
import { Component, OnInit, ViewChild, AfterViewInit, AfterContentInit, OnChanges, AfterViewChecked, AfterContentChecked, OnDestroy, SimpleChanges } from '@angular/core';

@Component({
  templateUrl: './logement.component.html',
  styleUrls: ['./logement.component.scss']
})
export class LogementComponent {
  //Permet de récupérer une instance de l'élement de la vue
  @ViewChild(MTabsDirective, {static: false}) mTabs: MTabsDirective;
  user: string = null;

  mTabInstance: M.Tabs = null;

  constructor(private httpClient: HttpClient) { }
  
  ngOnInit() {
  }
}
