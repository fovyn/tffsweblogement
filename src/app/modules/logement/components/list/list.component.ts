import { Observable } from 'rxjs';
import { SessionService } from './../../../user/services/session.service';
import { Component, ContentChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { BasketSessions } from 'src/app/modules/user/models/basket.model';

@Component({
  selector: 'logement-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, AfterViewInit {
  // static => Indique que le chargement de l'élement est disponible dans le ngAfterViewInit
  @ContentChild("tableHeader", {static: false}) tr: ElementRef<HTMLTableRowElement>;

  basket$: Observable<BasketSessions>
  constructor(private sessionService: SessionService) { }

  ngOnInit(): void {
    this.basket$ = this.sessionService.basket$;
  }

  ngAfterViewInit() {
    console.log(this.tr);
  }

}
