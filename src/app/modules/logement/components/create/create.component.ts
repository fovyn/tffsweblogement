import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LogementWithAddressForm } from '../../forms/logement.form';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'logement-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group(LogementWithAddressForm);
  }

  onSubmitAction() {
    console.log(this.form.value);
    // if (this.form.valid) {
      const logement = this.form.value;
      delete logement.address;
      this.httpClient.post("https://localhost:8000/api/logement/", logement.logement, {
        headers: new HttpHeaders({'Authorization': `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MDIxNTY0ODAsImV4cCI6MTYwMjE2MDA4MCwicm9sZXMiOlsiUk9MRV9VU0VSIiwiR1JPVVBfQURNSU4iLCJST0xFX0FETUlOIiwiUk9MRV9ST0xFX0NSRUFUT1IiLCJHUk9VUF9VU0VSIl0sInVzZXJuYW1lIjoiYWRtaW4ifQ.ebcyPsRFVnGUFNLRYoE04y_-HwS8Pxo5LJkXCW0b2AVBIgQcgYzOmhoq9Ec6N9nYuM7IHDs84pqanwPuNhvtdU1PPLzh3Xn4cyRMxEq_mHtio-Y_VRon9JEDZoy2hVdBDvCuswi2YmLS8b7A7hpLEmZhhMn28P2EzZA96Qaf7VICsnVLixa-qESy4qVuJBzOpL5IP2kfFb4LUC8G_PX--wX2q1E2JzZc0Rg7ZXVHfIhxz8Akw93Hxmr8MUT7JDc1h0H7l9UrHwLdQXQt8ANXrwkHsRsPvMUCSg9Pfcry4p50hN3VtWNPw8nEyEEt4OzmgSBnyNVeuGJ8zRrO6UgxHQ`})
      })
        .subscribe(data => console.log(data), err => console.log(err))
    // }
  }
}
