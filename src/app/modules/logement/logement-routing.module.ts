import { ListComponent } from './components/list/list.component';
import { LogementComponent } from './components/logement/logement.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: LogementComponent, children: [
    { path: 'list', component: ListComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogementRoutingModule { }
