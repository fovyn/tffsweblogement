import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonneRoutingModule } from './personne-routing.module';
import { PersonneComponent } from './components/personne/personne.component';


@NgModule({
  declarations: [PersonneComponent],
  imports: [
    CommonModule,
    PersonneRoutingModule
  ]
})
export class PersonneModule { }
