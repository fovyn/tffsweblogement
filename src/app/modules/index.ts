import { PersonneModule } from './personne/personne.module';
import { SharedModule } from './shared/shared.module';
import { LogementModule } from './logement/logement.module';


export { SharedModule, LogementModule, PersonneModule};