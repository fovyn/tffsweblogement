import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, Output, OnDestroy } from '@angular/core';
import * as M from 'materialize-css';

@Directive({
  selector: '[mTabs]'
})
export class MTabsDirective implements AfterViewInit, OnDestroy {
  @Input('mTabs') options: Partial<M.TabsOptions> = {};
  @Output('mInstance') mInstanceEmitter: EventEmitter<M.Tabs> = new EventEmitter<M.Tabs>();


  private _instance: M.Tabs;
  private _links: Map<HTMLAnchorElement, string> = new Map<HTMLAnchorElement, string>();

  constructor(private elRef: ElementRef<HTMLUListElement>) {
  }

  ngAfterViewInit() {
    this.instance = M.Tabs.init(this.nativeElement, this.options);
    this.mInstanceEmitter.emit(this.instance);

    this.getLinks();

    this._links.forEach((id: string, htmlLink: HTMLAnchorElement) => {
      htmlLink.addEventListener('click', () => this.instance.select(id));
    })
  }
  ngOnDestroy() {
    this.instance.destroy();
  }

  getLinks() {
    const anchors = this.nativeElement.querySelectorAll("a");

    //REGEX => expression régulière => (.*)\#(\w+) correspond à "(tu prends n'importe quel caractère 0 à n fois) jusqu'a ce que tu trouve une # suivi d'un mots composé de plusieurs charactère"
    //Tutoriel vers les regex => https://regexone.com
    // anchor.href => http://localhost:4200/#test1 => $1 = http://localhost:4200/; $2 = test1
    anchors.forEach(anchor => {
      const id: string = anchor.href.replace(/(.*)\#(\w+)/, '\#$2');
      this._links.set(anchor, id);
      // ids.push(anchor.href.replace(/(.*)\#(\w+)/, '\#$2'));
    });
  }

  get Links(): Map<HTMLAnchorElement, string> {
    return this._links;
  }
  get instance(): M.Tabs {
    return this._instance;
  }
  set instance(v: M.Tabs) {
    this._instance = v;
  }

  get nativeElement(): HTMLUListElement {
    return this.elRef.nativeElement;
  }

}
