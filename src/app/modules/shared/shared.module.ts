import { FormModule } from './modules/form/form.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MTabsDirective } from './directives/m-tabs.directive';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [MTabsDirective],
  imports: [
    CommonModule,
    FormModule,
  ],
  exports: [MTabsDirective, FormModule]
})
export class SharedModule { }
