import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from '../modules/user/services/session.service';

@Injectable({
    'providedIn': 'root'
})
export class ApiInterceptor implements HttpInterceptor {

    token: string;
    constructor(private sessionService: SessionService) {
        this.sessionService.token$.subscribe(token => this.token = token);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const newReq = req.clone({headers: new HttpHeaders({'Authorization': `Bearer ${this.token}`})});

        return next.handle(newReq);
    }

}