import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    'providedIn': 'root'
})
export class CorsInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // const newReq = req.clone({headers: new HttpHeaders({'Access-Control-Allow-Origin': '*'})});

        // return next.handle(newReq);

        return next.handle(req);
    }

}