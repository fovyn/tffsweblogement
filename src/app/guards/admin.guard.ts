import { SessionService } from './../modules/user/services/session.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  roles: any[];

  constructor(private sessionService: SessionService) {
    this.sessionService.roles$.subscribe(data => this.roles = data);
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log(this.roles);
      const finded = this.roles.find(r => r === "ROLE_ADMIN" || r === "ROLE_GROUP_ADMIN");
    return finded != null;
  }
  
}
