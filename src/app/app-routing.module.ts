import { AdminGuard } from './guards/admin.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components';
import { UserGuardGuard } from './guards/user-guard.guard';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'logements', canActivate: [UserGuardGuard], loadChildren: () => import('./modules').then(m => m.LogementModule)},
  { path: 'personnes', canActivate: [AdminGuard], loadChildren: () => import('./modules').then(m => m.PersonneModule)},
  { path: '', loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
